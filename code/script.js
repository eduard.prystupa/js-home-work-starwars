const CLASS_CONTAINER = 'container';

function loadJsonAndOutResult(url) {
    const swc = new XMLHttpRequest();

    swc.open('GET', url)
    swc.send()


    console.log(swc);

    swc.onreadystatechange = () => {
        console.log("1 OUT URL " + url);
        if (swc.readyState === 4 && swc.status === 200) {
            console.log("2 OUT URL " + url);
            //console.log("start");
            //console.log(JSON.parse(swc.responseText))
            //console.log("end");
            showData(JSON.parse(swc.responseText))
        }
    }
}

window.addEventListener('DOMContentLoaded', (url) => {
    loadJsonAndOutResult('https://swapi.dev/api/people');
})

function showData(data) {
    const cont = document.createElement('div');
    cont.classList.add(CLASS_CONTAINER)

    data.results.forEach((el) => {
        const card = document.createElement('div');
        card.classList.add("card");
        card.style.width = 18 + "rem";



        const cardBody = document.createElement('div');
        cardBody.classList.add('card-body');

        const cardTitle = document.createElement('h5');
        cardTitle.classList.add('card-title');

        const cardSubtitle = document.createElement('h6');
        cardSubtitle.classList.add('card-subtitle')

        const cardText = document.createElement('p');
        cardText.classList.add('card-text');
        //films
            const cardLink = document.createElement('a');
            cardLink.classList.add('card-link');
            cardLink.addEventListener('click', ()=>{
                cardLink.innerText = "Films: " + el.films;

            })

        //const

        cardTitle.innerText = "Ім'я: " + el.name;
        cardSubtitle.innerText = 'Планета: ' + el.homeworld;
        cardText.innerText = "Народився: " + el.birth_year;
        cardLink.innerText = "Інфо фільмів";


        card.append(cardBody);
        cardBody.append(cardTitle, cardSubtitle, cardText, cardLink);
        cont.append(card)

    })

    document.body.append(cont)
}
const list = document.querySelector('ul')

list.addEventListener('click',(e) => {
    console.log(e.target);
    document.querySelector("." + CLASS_CONTAINER).remove();

    /*list.childNodes.forEach((el, index) => {
        //console.log(index + " " + el + " " + el === e.target)

    })*/

    // https://swapi.dev/api/people/?page=7

    //console.log(e.target.parentElement)
    let index = Array.prototype.indexOf.call(list.childNodes, e.target.parentElement);
    index = (index / 2) + 0.5;

    let url = "https://swapi.dev/api/people/?page=" + index;


    //console.log(url)

    loadJsonAndOutResult(url);



    //console.log(data)
})
